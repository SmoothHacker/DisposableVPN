# DisposableVPN
On Demand VPN Service based on Open Source Technologies

## Details
DisposableVPN uses the Digital Ocean API to create OpenVPN endpoints on demand. What do we mean when we say dispoable? It's truely disposable! That means that hardly any data is preserved across sessions. In fact when you create the endpoint that's when we generate the encryption keys that allows you to connect to the endpoint. Once that happens, the keys are then downloaded to your local machine and deleted from the endpoint.

## How to Install

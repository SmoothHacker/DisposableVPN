import com.myjeeva.digitalocean.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.io.*;

public class App {
  private static Scanner inpt = new Scanner(System.in);

  public static void main( String[] args ) {
    final String DO_TOKEN;
    System.out.print("Welcome to DisposableVPN.\nHave you created a token and pasted it inside of the \'Digital" +
      "Ocean_Token.env\' file? ");
    if(inpt.next().toLowerCase().equals("y")) {
      try {
        DO_TOKEN = tokenParser();
        menu(DO_TOKEN);
      } catch(Exception e) {
        System.out.println("File Not Found");
      }
    } else {
      System.out.println("Please create an API token and paste it into the \'DigitalOcean_Token.env\' file " +
        "\nDisposableVPN needs that token to function");
      System.exit(0);
    }


  }

  private static String tokenParser() throws IOException {
    return new String(Files.readAllBytes(Paths.get("DigitalOcean_Token.env")));
  }

  private static void menu(String DO_TOKEN) {
    System.out.println("\t1. Create VPN");
  }
}

import com.myjeeva.digitalocean.DigitalOcean;
import com.myjeeva.digitalocean.impl.DigitalOceanClient;
import com.myjeeva.digitalocean.pojo.Droplet;

import java.util.Scanner;

public class VPN_Creator {
  private String name;
  private String Size; // Set permanently by slug value
  private String Region;
  private String Image; // Set permanently by Image ID
  private DigitalOcean apiClient;
  private Scanner inpt = new Scanner(System.in);

  public VPN_Creator(String Region, final String DO_TOKEN) {
    this.name = "DisposibleVPN-EndPoint";
    this.Region = Region;
    this.Image = "ubuntu-16-04-x64";
    this.Size = "s-1vcpu-1gb"; // $5/month plan
    apiClient = new DigitalOceanClient(DO_TOKEN);
  }

  public boolean createVPN() {

    return true;
  }

  void regionChanger() {
    String response;

    String[] availableRegions = {"NYC1", "NYC3", "AMS3", "SFO2", "SGP1", "LON1", "FRA1", "TOR1", "BLR1"}; // TODO Research to see if we can fetch this dynamically
    System.out.println("Available Regions include:");
    for(String Region: availableRegions)
      System.out.println("\t" + Region);
    System.out.print("Response: ");
    response = inpt.next().trim(); // Make sure errors don't come from whitespace;

    for(String Region: availableRegions) {
      if(response.equals(Region)) {
        Region = response;
        System.out.println("Region has been successfully changed to " + Region);
        break; // Exits function early
      }
    }
    System.out.println("Region not found...\nType exactly as it appears.");

  }
  /*
  * Methods needed
  *   Droplet Creator()
  *   UserInput Parser
  *
  * */
}
